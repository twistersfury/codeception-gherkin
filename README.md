[![codecov](https://codecov.io/gl/twistersfury/codeception-gherkin/branch/master/graph/badge.svg)](https://codecov.io/gl/twistersfury/codeception-gherkin)
[![pipelines](https://gitlab.com/twistersfury/codeception-gherkin/badges/master/pipeline.svg)](https://codecov.io/gl/twistersfury/codeception-gherkin)

# Codeception Gherkin Page Object Helper

This project provides the default steps for the various [Codeception](https://codeception.com) modules. It leverages the concept of [Page Object Models](https://medium.com/tech-tajawal/page-object-model-pom-design-pattern-f9588630800b) to allow you to easily configure specific elements without the need to put these directly in your feature files.
 
## Steps
Add any of the included traits to your Tester Class. After adding, you can use the Codeception `codecept gherkin:steps` to see all options.

### Available Traits

- TwistersFury\Codeception\Gherkin\Module\Db
- TwistersFury\Codeception\Gherkin\Module\WebDriver

##### Example Steps:

###### haveInDatabase
```gherkin
Scenario: Do Something
  Given I have in database "table"
    | field1 | field2 | field3 |
    | value 1| value 2| value #|
```

###### seeInDatabase
```gherkin
Scenario: Do Something
  Then I see in database "table"
    | field1 | field2 | field3 |
    | value 1| value 2| value #|
```

#### PageObject
Register your pages in the Codeception bootstrap file. The page name will be the identifier used in your gherkin feature files. The class name is the class to initialize. As part of your Scenario, use the `I am on page` step to initialize the page.

Your Page Objects must extend `TwistersFury\Codeception\Gherkin\Page\AbstractPage` and define the methods:

- `getUrl` the relative url of the page.
- `getElementMap` The map of elements (See Below)
- `getDefaultElement` The default HTML element.

#### Element Maps
Element maps are used to define elements on the page. The selection system uses the Codeception `Location` class to build the selector in the command. If an element is requested that doesn't exist, then the requested is returned. Possible values for a mapping are:

- `method` Location Method To Use. Options are:
  - `find` - Default - 
  - `href`
  - `name`
- `attributes` - Defines Search In find
- `element` - Defines HTML element in `find` and `name`. Defaults to `getDefaultElement`
- `url` - Used in `href` method.

##### Examples
`bootstrap.php`
```php
<?php

use My\Pages\AdminPage;
use My\Pages\Signup;
use My\Pages\ProfilePage
use TwistersFury\Codeception\Gherkin\Page\Factory;

Factory::reset();
Factory::getInstance()->addPages(
   [
        'admin'  => AdminPage::class,
        'signup' => SignUp::class
   ]
);

Factory::getInstance()->addPage(
    'profile', ProfilePage::class
);
```

`tests/_support/Pages/Signup.php` 
```php
<?php

namespace My\Pages;

class Signup {
    protected function getDefaultElement(): string 
    {
        return 'input';
    }
    
    public function getUrl(): string 
    {
        return '/signup';
    }

    protected function getElementMap(): array
    {
        return [
            'signup' => [
                'element' => 'button',
                'attributes' => [
                    'class' => 'signup'
                ]
            ],
            'forgot-my-pass' => [
                'method' => 'href',
                'url'    => '/forgot-pass/'
            ],
            'username' => [
                'method' => 'name'
            ]
        ]; 
    }
}
```