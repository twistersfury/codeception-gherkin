<?php
/**
 * Copyright (C) 2019 Twister's Fury.
 * Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
 */

namespace TwistersFury\Codeception\Gherkin\Traits\Module;

use Behat\Gherkin\Node\TableNode;

/**
 * Trait Db
 *
 * @package TwistersFury\Codeception\Gherkin\Traits\Module
 * @method seeInDatabase(string $table, array $query)
 * @method haveInDatabase(string $table, array $record);
 */
trait Db
{
    /**
     * @param $table
     * @param $data
     * @return self
     *
     * @Then I see in database :table
     */
    public function iSeeInDatabase(string $table, TableNode $data) : self
    {
        foreach ($data as $node) {
            $this->seeInDatabase(
                $table,
                $node
            );
        }

        return $this;
    }

    /**
     * @param string                        $tableName
     * @param \Behat\Gherkin\Node\TableNode $data
     *
     * @return \TwistersFury\Codeception\Gherkin\Traits\Module\Db
     * @Given I have in database :tableName
     */
    public function iHaveInDatabase(string $tableName, TableNode $data) : self
    {
        foreach ($data as $node) {
            $this->haveInDatabase($tableName, $node);
        }

        return $this;
    }
}
