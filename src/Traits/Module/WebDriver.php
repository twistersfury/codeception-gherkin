<?php
/**
 * Copyright (C) 2019 Twister's Fury.
 * Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
 */

namespace TwistersFury\Codeception\Gherkin\Traits\Module;

use Behat\Gherkin\Node\TableNode;
use TwistersFury\Codeception\Gherkin\Page\AbstractPage;
use TwistersFury\Codeception\Gherkin\Page\Factory;

/**
 * Trait WebDriver
 *
 * @package TwistersFury\Codeception\Gherkin\Traits\Module
 * @method amOnPage(string $url)
 * @method fillField(string $selector, $value)
 * @method scrollTo(string $selector)
 * @method checkOption(string $selector)
 * @method click(string $selector)
 * @method seeElement(string $selector)
 * @method waitForElement(string $selector, int $timeout= 5)
 * @method seeOutput(string $output)
 */
trait WebDriver
{
    private $currentPage = null;

    public function grabCurrentPage() : AbstractPage
    {
        if ($this->currentPage === null) {
            $this->currentPage = Factory::getInstance()->getLastPage();
        }

        return $this->currentPage;
    }

    /**
     * @Given I am on page :pageName
     */
    public function iAmOnPage(string $pageName) : self
    {
        $this->currentPage = Factory::getInstance()->get($pageName);

        $this->amOnPage($this->grabCurrentPage()->getUrl());

        return $this;
    }

    /**
     * @Given I am on url :url of page :pageName
     */
    public function iAmOnUrlOfPage(string $url, string $pageName) : self
    {
        $this->currentPage = Factory::getInstance()->get($pageName);

        $this->amOnPage($url);

        return $this;
    }

    /**
     * @When I fill fields
     */
    public function iFillFields(TableNode $tableNode) : self
    {
        foreach ($tableNode as $node) {
            $this->fillField(
                $this->grabCurrentPage()->getSelector($node['field']),
                $node['value']
            );
        }

        return $this;
    }

    /**
     * @When I scroll to :optionName
     * @param string $optionName
     * @return self
     */
    public function iScrollTo(string $optionName) : self
    {
        $this->scrollTo($this->grabCurrentPage()->getSelector($optionName));

        return $this;
    }

    /**
     * @When I check option :optionName
     * @param string $optionName
     * @return self
     */
    public function iCheckOption(string $optionName) : self
    {
        $this->checkOption($this->grabCurrentPage()->getSelector($optionName));

        return $this;
    }

    /**
     * @When I click :buttonSelector
     * @param string $buttonSelector
     * @return self
     */
    public function iClick(string $buttonSelector) : self
    {
        $this->click($this->grabCurrentPage()->getSelector($buttonSelector));

        return $this;
    }

    /**
     * @When I wait for element :elementName
     * @param string $elementName
     * @return self
     */
    public function iWaitForElement(string $elementName) : self
    {
        $this->waitForElement($this->grabCurrentPage()->getSelector($elementName));

        return $this;
    }

    /**
     * @param string $elementName
     * @Then I see element :elementName
     * @return self
     */
    public function iSeeElement(string $elementName) : self
    {
        $this->seeElement($this->grabCurrentPage()->getSelector($elementName));

        return $this;
    }

    /**
     * @param string $output
     *
     * @return self
     * @Then I see output :output
     */
    public function iSeeOutput(string $output) : self
    {
        $this->seeOutput($output);

        return $this;
    }
}
