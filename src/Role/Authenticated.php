<?php
/**
 * Copyright (C) 2019 Twister's Fury.
 * Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
 */

namespace TwistersFury\Codeception\Gherkin\Role;

use TwistersFury\Codeception\Gherkin\Role\Interfaces\Authenticated as AuthenticatedInterface;

abstract class Authenticated extends AbstractRole implements AuthenticatedInterface
{
    public function isLoggedIn() : bool
    {
        return true;
    }

    /**
     * @Given I am logged in
     */
    public function iAmLoggedIn() : self
    {
        return $this->amLoggedIn();
    }

    abstract protected function amLoggedIn() : self;

    protected function onConstruct() : AbstractRole
    {
        if (!$this->isLoggedIn()) {
            $this->amLoggedIn();
        }

        return parent::onConstruct();
    }
}
