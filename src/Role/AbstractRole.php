<?php
/**
 * Copyright (C) 2019 Twister's Fury.
 * Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
 */

namespace TwistersFury\Codeception\Gherkin\Role;

use Codeception\Actor;

use TwistersFury\Codeception\Gherkin\Role\Interfaces\Role;

abstract class AbstractRole implements Role
{
    /** @var Actor */
    private $tester = null;

    public function __construct(Actor $tester)
    {
        $this->setActor($tester)->onConstruct();
    }

    public function getActor() : Actor
    {
        return $this->tester;
    }

    protected function onConstruct() : self
    {
        //Place Holder
        return $this;
    }

    protected function setActor(Actor $tester) : self
    {
        $this->tester = $tester;

        return $this;
    }
}
