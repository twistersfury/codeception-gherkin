<?php
/**
 * Copyright (C) 2019 Twister's Fury.
 * Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
 */

namespace TwistersFury\Codeception\Gherkin\Role;

use TwistersFury\Codeception\Gherkin\Role\Interfaces\Authenticated as AuthenticatedInterface;

class UnAuthenticated extends AbstractRole implements AuthenticatedInterface
{
    public function isLoggedIn() : bool
    {
        return false;
    }

    /**
     * @Given I am logged out
     */
    public function iAmLoggedOut() : self
    {
        return $this->amLoggedOut();
    }

    protected function onConstruct() : AbstractRole
    {
        if ($this->isLoggedIn()) {
            $this->amLoggedOut();
        }

        return parent::onConstruct();
    }

    protected function amLoggedOut() : self
    {
        return $this;
    }
}
