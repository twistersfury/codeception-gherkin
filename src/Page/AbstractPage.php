<?php
/**
 * Copyright (C) 2019 Twister's Fury.
 * Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
 */

namespace TwistersFury\Codeception\Gherkin\Page;

use Codeception\Util\Locator;
use LogicException;

abstract class AbstractPage
{
    abstract public function getUrl() : string;

    public function getBaseUrl() : string
    {
        return '//' . getenv('ENV_BASE_DOMAIN') . '/';
    }

    public function getSelector(string $selectorIdent) : string
    {
        $elementName = strtolower($selectorIdent);

        if (!isset($this->getElementMap()[$elementName])) {
            return $selectorIdent;
        }

        $elementMap = $this->getElementMap()[$elementName];

        $methodName = 'generate' . ($elementMap['method'] ?? 'find');
        if (!method_exists($this, $methodName)) {
            throw new LogicException('Method Type Not Defined');
        }

        return $this->{$methodName}($elementMap);
    }
    abstract protected function getElementMap() : array;
    abstract protected function getDefaultElement() : string;

    protected function generateFind(array $elementMap) : string
    {
        $elementMap = array_merge(
            [
                'element'    => $this->getDefaultElement(),
                'attributes' => []
            ],
            $elementMap
        );

        return Locator::find(
            $elementMap['element'],
            $elementMap['attributes']
        );
    }

    protected function generateHref(array $elementMap) : string
    {
        if (!isset($elementMap['url'])) {
            throw new \LogicException('URL Not Specified');
        }

        return Locator::href($elementMap['url']);
    }

    protected function generateName(array $elementMap) : string
    {
        return $elementMap['element'] ?? $this->getDefaultElement();
    }
}
