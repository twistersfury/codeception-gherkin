<?php
/**
 * Copyright (C) 2019 Twister's Fury.
 * Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
 */

namespace TwistersFury\Codeception\Gherkin\Page;

use LogicException;

class Factory
{
    protected $pageMap = [
        'general' => General::class
    ];
    private static $instance = null;

    private $pageObjects = [];

    private $lastPage = null;

    final protected function __construct()
    {
        //Nothing To See Here - Singleton
    }

    final public static function getInstance() : Factory
    {
        if (static::$instance === null) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    final public static function reset() : void
    {
        static::$instance = null;
    }

    public function get(string $pageName) : AbstractPage
    {
        $pageName = strtolower($pageName);

        $this->lastPage = $pageName;

        if (isset($this->pageObjects[$pageName])) {
            return $this->pageObjects[$pageName];
        }

        $this->pageObjects[$pageName] = $this->createPage($pageName);

        return $this->pageObjects[$pageName];
    }

    public function getLastPage() : AbstractPage
    {
        return $this->get($this->lastPage ?? '');
    }

    public function addPages(array $pages) : self
    {
        foreach ($pages as $pageName => $page) {
            $this->addPage($pageName, $page);
        }

        return $this;
    }

    public function addPage(string $pageName, string $className) : self
    {
        if (!is_subclass_of($className, AbstractPage::class)) {
            throw new LogicException('Not Instance Of Abstract Page');
        }

        $this->pageMap[$pageName] = $className;

        return $this;
    }

    public function getDefaultPage() : string
    {
        return General::class;
    }

    private function createPage(string $pageName) : AbstractPage
    {
        $pageName = strtolower($pageName);

        if (!isset($this->pageMap[$pageName])) {
            $pageName = $this->getDefaultPage();
        }

        $className = $this->pageMap[$pageName] ?? General::class;

        return new $className();
    }
}
