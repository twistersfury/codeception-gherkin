<?php
/**
 * Copyright (C) 2019 Twister's Fury.
 * Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
 */

namespace TwistersFury\Codeception\Gherkin\Page;

class General extends AbstractPage
{
    public function getUrl() : string
    {
        return '/';
    }

    protected function getElementMap() : array
    {
        return [

        ];
    }

    protected function getDefaultElement() : string
    {
        return 'a';
    }
}
