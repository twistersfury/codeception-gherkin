<?php
/**
 * Copyright (C) 2019 Twister's Fury.
 * Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
 */

namespace TwistersFury\Codeception\Gherkin\Tests\Unit\Page;

use Codeception\Test\Unit;
use Generator;
use PHPUnit\Framework\MockObject\MockObject;
use TwistersFury\Codeception\Gherkin\Page\AbstractPage;

/**
 * Class AbstractPageTest
 *
 * @package TwistersFury\Codeception\Gherkin\Tests\Unit\Page
 * @property \UnitTester $tester
 */
class AbstractPageTest extends Unit
{
    /** @var AbstractPage|MockObject */
    private $testSubject = null;

    public function _before()
    {
        $this->testSubject = $this->getMockBuilder(AbstractPage::class)
            ->setMethods(['getElementMap', 'getDefaultElement'])
            ->getMockForAbstractClass();

        $this->testSubject->method('getDefaultElement')->willReturn('default-element');
    }

    public function testDefaultUrl()
    {
        putenv('ENV_BASE_DOMAIN=some.domain');
        $this->assertEquals('//some.domain/', $this->testSubject->getBaseUrl());
        putenv('ENV_BASE_DOMAIN');
    }

    public function testNoElementMap()
    {
        $this->testSubject->expects($this->once())->method('getElementMap')
            ->willReturn([]);

        $this->assertEquals('blah', $this->testSubject->getSelector('blah'));
    }

    public function testInvalidMethodType()
    {
        $this->tester->expectThrowable(
            new \LogicException('Method Type Not Defined'),
            function () {
                $this->testSubject->method('getElementMap')
                    ->willReturn(['blah' => ['method' => 'blah']]);

                $this->assertEquals('blah', $this->testSubject->getSelector('blah'));
            }
        );
    }

    public function testMissingUrl()
    {
        $this->tester->expectThrowable(
            new \LogicException('URL Not Specified'),
            function () {
                $this->testSubject->method('getElementMap')
                    ->willReturn(['blah' => ['method' => 'href']]);

                $this->assertEquals('blah', $this->testSubject->getSelector('blah'));
            }
        );
    }

    /**
     * @dataProvider _dpGetSelector
     */
    public function testGetSelector(array $elementData, string $selectorResult)
    {
        $this->testSubject->method('getElementMap')
            ->willReturn(
                [
                    'random' => $elementData
                ]
            );

        $this->assertEquals($selectorResult, $this->testSubject->getSelector('random'));
    }

    public function _dpGetSelector() : Generator
    {
        yield 'Test Find' => [
            [
                'element'    => 'a',
                'attributes' => [
                    'name' => 'blah'
                ]
            ],
            '//a[@name = \'blah\']'
        ];

        yield 'Test Href' => [
            [
                'method'  => 'href',
                'element' => 'a',
                'url'     => '/'
            ],
            '//a[@href=normalize-space(\'/\')]'
        ];

        yield 'Test Name' => [
            [
                'element' => 'blah',
                'method'  => 'name'
            ],
            'blah'
        ];

        yield 'Test Default Element' => [
            [
                'method' => 'name'
            ],
            'default-element'
        ];
    }
}
