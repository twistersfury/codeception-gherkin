<?php
/**
 * Copyright (C) 2019 Twister's Fury.
 * Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
 */

namespace TwistersFury\Codeception\Gherkin\Tests\Unit\Page;

use Codeception\Test\Unit;
use ReflectionMethod;
use TwistersFury\Codeception\Gherkin\Page\General;
use UnitTester;

/**
 * Class GeneralTest
 *
 * @package TwistersFury\Codeception\Gherkin\Tests\Unit\Page
 * @property UnitTester $tester;
 */
class GeneralTest extends Unit
{
    /** @var General */
    private $testSubject = null;

    public function _before()
    {
        $this->testSubject = new General();
    }

    public function testUrl()
    {
        $this->assertEquals('/', $this->testSubject->getUrl());
    }

    public function testElementMap()
    {
        $methodReflection = new ReflectionMethod(General::class, 'getElementMap');
        $methodReflection->setAccessible(true);

        $this->assertEquals([], $methodReflection->invoke($this->testSubject));
    }

    public function testDefaultElement()
    {
        $methodReflection = new ReflectionMethod(General::class, 'getDefaultElement');
        $methodReflection->setAccessible(true);

        $this->assertEquals('a', $methodReflection->invoke($this->testSubject));
    }
}
