<?php
/**
 * Copyright (C) 2019 Twister's Fury.
 * Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
 */

namespace TwistersFury\Codeception\Gherkin\Tests\Unit\Page;

use Codeception\Test\Unit;
use LogicException;
use stdClass;
use TwistersFury\Codeception\Gherkin\Page\AbstractPage;
use TwistersFury\Codeception\Gherkin\Page\Factory;
use TwistersFury\Codeception\Gherkin\Page\General;

/**
 * Class FactoryTest
 *
 * @package TwistersFury\Codeception\Gherkin\Tests\Unit\Page
 * @property \UnitTester $tester
 */
class FactoryTest extends Unit
{
    /** @var Factory */
    private $testSubject = null;

    public function _before()
    {
        $this->testSubject = Factory::getInstance();
    }

    public function _after()
    {
        Factory::reset();
        ;
    }

    public function testGet()
    {
        $mockPage = $this->getMockBuilder(AbstractPage::class)
            ->getMockForAbstractClass();

        $this->testSubject->addPages(
            [
                'random' => get_class($mockPage)
            ]
        );

        $this->assertInstanceOf(get_class($mockPage), $this->testSubject->get('random'));
        $this->assertInstanceOf(get_class($mockPage), $this->testSubject->getLastPage());
    }

    public function testGetDefault()
    {
        $this->assertInstanceOf(General::class, $this->testSubject->get('dummy'));
    }

    public function testAddInvalidPage()
    {
        $this->tester->expectThrowable(
            new LogicException('Not Instance Of Abstract Page'),
            function () {
                $this->testSubject->addPage('blah', stdClass::class);
            }
        );
    }
}
