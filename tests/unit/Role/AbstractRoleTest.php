<?php
/**
 * Copyright (C) 2019 Twister's Fury.
 * Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
 */

namespace TwistersFury\Codeception\Gherkin\Tests\Unit\Role;

use Codeception\Actor;
use Codeception\Test\Unit;
use PHPUnit\Framework\MockObject\MockObject;
use TwistersFury\Codeception\Gherkin\Role\AbstractRole;

class AbstractRoleTest extends Unit
{
    /** @var AbstractRole|MockObject */
    private $testSubject = null;

    public function _before()
    {
        $this->testSubject = $this->getMockBuilder(AbstractRole::class)
            ->setConstructorArgs(
                [
                    $this->getMockBuilder(Actor::class)
                        ->disableOriginalConstructor()
                        ->getMockForAbstractClass()
                ]
            )
            ->getMockForAbstractClass();
    }

    public function testActor()
    {
        $this->assertInstanceOf(
            Actor::class,
            $this->testSubject->getActor()
        );
    }
}
