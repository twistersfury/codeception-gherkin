<?php
/**
 * Copyright (C) 2019 Twister's Fury.
 * Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
 */

namespace TwistersFury\Codeception\Gherkin\Tests\Unit\Role;

use Codeception\Actor;
use Codeception\Test\Unit;
use PHPUnit\Framework\MockObject\MockObject;
use TwistersFury\Codeception\Gherkin\Role\UnAuthenticated;

class UnAuthenticatedTest extends Unit
{
    /** @var UnAuthenticated|MockObject */
    private $testSubject = null;

    public function _before()
    {
        $mockActor = $this->getMockBuilder(Actor::class)
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $this->testSubject = $this->getMockBuilder(UnAuthenticated::class)
            ->setConstructorArgs(
                [
                    $mockActor
                ]
            )
            ->setMethods([])
            ->getMockForAbstractClass();
    }

    public function testIsLoggedIn()
    {
        $this->assertFalse($this->testSubject->isLoggedIn());
    }

    public function testIAmLoggedOut()
    {
        $this->tester->seeGherkinStep('I am logged out');
        $this->testSubject->iAmLoggedOut();
    }

    public function testAutoLogIn()
    {
        $this->testSubject = $this->getMockBuilder(UnAuthenticated::class)
            ->disableOriginalConstructor()
            ->setMethods(['isLoggedIn'])
            ->getMockForAbstractClass();

        $this->testSubject->method('isLoggedIn')
            ->willReturn(true);

        $reflectionMethod = new \ReflectionMethod(UnAuthenticated::class, 'onConstruct');
        $reflectionMethod->setAccessible(true);

        $this->assertSame($this->testSubject, $reflectionMethod->invoke($this->testSubject));
    }
}
