<?php
/**
 * Copyright (C) 2019 Twister's Fury.
 * Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
 */

namespace TwistersFury\Codeception\Gherkin\Tests\Unit\Role;

use Codeception\Actor;
use Codeception\Test\Unit;
use PHPUnit\Framework\MockObject\MockObject;
use TwistersFury\Codeception\Gherkin\Role\Authenticated;

/**
 * Class AuthenticatedTest
 *
 * @package TwistersFury\Codeception\Gherkin\Tests\Unit\Role
 * @property \UnitTester $tester
 */
class AuthenticatedTest extends Unit
{
    /** @var Authenticated|MockObject */
    private $testSubject = null;

    public function _before()
    {
        $mockActor = $this->getMockBuilder(Actor::class)
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $this->testSubject = $this->getMockBuilder(Authenticated::class)
            ->setConstructorArgs(
                [
                    $mockActor
                ]
            )
            ->setMethods([])
            ->getMockForAbstractClass();
    }

    public function testIsLoggedIn()
    {
        $this->assertTrue($this->testSubject->isLoggedIn());
    }

    public function testIAmLoggedIn()
    {
        $this->tester->seeGherkinStep('I am logged in');
        $this->testSubject->iAmLoggedIn();
    }

    public function testAutoLogIn()
    {
        $this->testSubject = $this->getMockBuilder(Authenticated::class)
            ->disableOriginalConstructor()
            ->setMethods(['isLoggedIn'])
            ->getMockForAbstractClass();

        $this->testSubject->method('isLoggedIn')
            ->willReturn(false);

        $reflectionMethod = new \ReflectionMethod(Authenticated::class, 'onConstruct');
        $reflectionMethod->setAccessible(true);

        $this->assertSame($this->testSubject, $reflectionMethod->invoke($this->testSubject));
    }
}
