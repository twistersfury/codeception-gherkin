<?php
/**
 * Copyright (C) 2019 Twister's Fury.
 * Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
 */

namespace TwistersFury\Codeception\Gherkin\Tests\Unit\Traits\Module;

use Behat\Gherkin\Node\TableNode;
use Codeception\Test\Unit;
use PHPUnit\Framework\MockObject\MockObject;
use TwistersFury\Codeception\Gherkin\Traits\Module\Db;
use UnitTester;

/**
 * Class DbTest
 *
 * @package TwistersFury\Codeception\Gherkin\Tests\Unit\Module
 * @property UnitTester $tester
 */
class DbTest extends Unit
{
    /** @var Db|MockObject */
    private $testSubject = null;

    public function _before()
    {
        $this->testSubject = $this->getMockBuilder(Db::class)
            ->setMethods(
                [
                    'seeInDatabase',
                    'haveInDatabase'
                ]
            )
            ->getMockForTrait();
    }

    public function testISeeInDatabase()
    {
        $this->testSubject->expects($this->once())->method('seeInDatabase')
            ->with('table', ['field' => 'a', 'value' => 'b']);

        $tableNode = new TableNode([['field', 'value'], ['a', 'b']]);

        $this->assertSame($this->testSubject, $this->testSubject->iSeeInDatabase('table', $tableNode));
    }

    public function testIHaveInDatabase()
    {
        $this->testSubject->expects($this->once())->method('haveInDatabase')
            ->with('table', ['field' => 'a', 'value' => 'b']);

        $tableNode = new TableNode([['field', 'value'], ['a', 'b']]);

        $this->assertSame($this->testSubject, $this->testSubject->iHaveInDatabase('table', $tableNode));
    }
}
