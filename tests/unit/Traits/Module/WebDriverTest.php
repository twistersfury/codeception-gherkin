<?php
/**
 * Copyright (C) 2019 Twister's Fury.
 * Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
 */

namespace TwistersFury\Codeception\Gherkin\Tests\Unit\Traits\Module;

use Behat\Gherkin\Node\TableNode;
use Codeception\Test\Unit;
use PHPUnit\Framework\MockObject\MockObject;
use TwistersFury\Codeception\Gherkin\Page\Factory;
use TwistersFury\Codeception\Gherkin\Tests\Support\Mock\Page\Testing;
use TwistersFury\Codeception\Gherkin\Traits\Module\WebDriver;

class WebDriverTest extends Unit
{
    /** @var WebDriver|MockObject */
    private $testSubject = null;

    public function _before()
    {
        $this->testSubject = $this->getMockBuilder(WebDriver::class)
            ->setMethods(
                [
                    'amOnPage',
                    'fillField',
                    'checkOption',
                    'waitForElement',
                    'seeElement',
                    'scrollTo',
                    'click',
                    'seeOutput'
                ]
                                  )
            ->getMockForTrait();

        Factory::getInstance()->addPage('random', Testing::class);
    }

    public function _after()
    {
        Factory::reset();
    }

    public function testIAmOnUrlOfPage()
    {
        $this->testSubject->expects($this->once())->method('amOnPage')
            ->with('/some/other/url');

        $this->assertSame(
            $this->testSubject,
            $this->testSubject->iAmOnUrlOfPage('/some/other/url', 'random')
        );
    }

    public function testIAmOnPage()
    {
        $this->testSubject->expects($this->once())->method('amOnPage')
            ->with('/some/page/url');

        $this->assertSame(
            $this->testSubject,
            $this->testSubject->iAmOnPage('random')
        );
    }

    public function testIFillFields()
    {
        $this->testSubject->expects($this->once())->method('fillField')
            ->with('a', 'b');

        $tableNode = new TableNode([['field', 'value'], ['a', 'b']]);

        $this->assertSame($this->testSubject, $this->testSubject->iFillFields($tableNode));
    }

    public function testIScrollTo()
    {
        $this->testSubject->expects($this->once())->method('scrollTo')
            ->with('a');

        $this->assertSame($this->testSubject, $this->testSubject->iScrollTo('a'));
    }

    public function testICheckOption()
    {
        $this->testSubject->expects($this->once())->method('checkOption')
            ->with('a');

        $this->assertSame($this->testSubject, $this->testSubject->iCheckOption('a'));
    }

    public function testIWaitForElement()
    {
        $this->testSubject->expects($this->once())->method('waitForElement')
            ->with('a');

        $this->assertSame($this->testSubject, $this->testSubject->iWaitForElement('a'));
    }

    public function testISeeElement()
    {
        $this->testSubject->expects($this->once())->method('seeElement')
            ->with('a');

        $this->assertSame($this->testSubject, $this->testSubject->iSeeElement('a'));
    }

    public function testIClick()
    {
        $this->testSubject->expects($this->once())->method('click')
            ->with('a');

        $this->assertSame($this->testSubject, $this->testSubject->iClick('a'));
    }

    public function testISeeOutput()
    {
        $this->testSubject->expects($this->once())->method('seeOutput')
            ->with('a');

        $this->assertSame($this->testSubject, $this->testSubject->iSeeOutput('a'));
    }
}
