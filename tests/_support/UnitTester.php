<?php
/**
 * Copyright (C) 2019 Twister's Fury.
 * Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
 */

class UnitTester extends \Codeception\Actor
{
    use _generated\UnitTesterActions;

    /**
     * Define custom actions here
     */

    public function seeGherkinStep(string $output)
    {
        $this->runShellCommand(
            'cd ' . codecept_root_dir() . 'tests/_data/e2e && ./../../../vendor/bin/codecept gherkin:steps acceptance'
        );

        $this->seeInShellOutput($output);
    }
}
