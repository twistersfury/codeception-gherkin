<?php
/**
 * Copyright (C) 2019 Twister's Fury.
 * Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
 */

namespace TwistersFury\Codeception\Gherkin\Tests\Support\Mock\Role;

class UnAuthenticated extends \TwistersFury\Codeception\Gherkin\Role\UnAuthenticated
{
}
