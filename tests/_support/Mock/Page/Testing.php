<?php
/**
 * Copyright (C) 2019 Twister's Fury.
 * Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
 */

namespace TwistersFury\Codeception\Gherkin\Tests\Support\Mock\Page;

use TwistersFury\Codeception\Gherkin\Page\AbstractPage;

class Testing extends AbstractPage
{
    public function getUrl() : string
    {
        return '/some/page/url';
    }

    protected function getDefaultElement() : string
    {
        return 'random';
    }

    protected function getElementMap() : array
    {
        return [
            'blah' => [
                'method' => 'name'
            ]
        ];
    }
}
