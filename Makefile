######################################################
#        SYSTEM MAKE FILE                            #
######################################################
# Use this to allow quick/easy modifications and/or  #
# updates to the software. Requires make to be       #
# installed.                                         #
#                                                    #
# Note: This assumes you already have PHP setup and  #
#   properly configured in your path environment     #
######################################################
# Windows                                            #
#                                                    #
# http://gnuwin32.sourceforge.net/downlinks/make.php #
######################################################
# OS X / Linux                                       #
#                                                    #
# Likely Already Installed. Otherwise use the os's   #
# package manager (IE: apt-get, brew, macports, etc) #
######################################################

CACHE_PATH=cache
BIN_PATH=bin
NODE_MODULES=node_modules

COMPOSER_URL=https://getcomposer.org/installer
PHPSTAN_URL=https://github.com/phpstan/phpstan/releases/download/0.10.2/phpstan.phar

TEST_EXTRA_OPTIONS ?= --ext DotReporter
CODECEPT_OPTIONS ?= --fail-fast $(TEST_EXTRA_OPTIONS)
TEST_ENABLE_COVERAGE=false
TEST_COVERAGE ?=

PHP_IDE_CONFIG = PHP_IDE_CONFIG='serverName=make.test'

DOCKER_IMAGE ?= registry.gitlab.com/twistersfury/codeception-gherkin/php
DOCKER_ENABLE ?= true
DOCKER_EXTRA_OPTIONS ?=

DEBUG_ENABLE ?= false

ifneq ($(TEST_ENABLE_COVERAGE),false)
	TEST_COVERAGE := --coverage --coverage-html
endif

ifeq ($(DOCKER_ENABLE),true)
	APP_DIRECTORY ?= /app
	DOCKER_COMMAND ?= docker run --rm -it -v $(CURDIR):$(APP_DIRECTORY) -w $(APP_DIRECTORY) --env $(PHP_IDE_CONFIG) $(DOCKER_EXTRA_OPTIONS) $(DOCKER_IMAGE)
	PHP_REMOTE_HOST := host.docker.internal
else
	APP_DIRECTORY ?= $(shell pwd)
	DOCKER_COMMAND :=
	PHP_REMOTE_HOST := 127.0.0.1
endif

ifneq ($(DEBUG_ENABLE),false)
	PHP_ARGS ?= -d zend_extension=xdebug.so -d xdebug.remote_enable=1 -d xdebug.remote_autostart=1 -d xdebug.remote_host=$(PHP_REMOTE_HOST)
endif

######################################################
#                 Default Run Command                #
######################################################
.PHONY: default
default: analyse

######################################################
#               Hard File Dependencies               #
######################################################

# Primary Cache Directory
$(CACHE_PATH):
	mkdir -p $(CACHE_PATH)

# Primary Bin Directory
$(BIN_PATH):
	mkdir -p $(BIN_PATH)

# Composer Dependencies
vendor: composer.json composer.lock
	make composer-install

composer.lock:
	make composer-install

# Binaries
vendor/codeception/codeception/codecept: $(BIN_PATH)/.build | vendor

$(BIN_PATH)/.build: $(shell find ./docker -type f -name "*") | $(BIN_PATH)
	docker build -t codecept-gherkin ./docker/php
	touch $@

$(BIN_PATH)/.docker-login: | $(BIN_PATH)
	docker login registry.gitlab.com
	touch $@

# Docker Installation
$(BIN_PATH)/.docker: | $(BIN_PATH)
	which docker || (echo "Docker Is Not Installed" && exit 1)
	touch $(BIN_PATH)/.docker

######################################################
#            Installation Dependencies               #
######################################################

.PHONY: clean composer install

install: vendor

clean:
	rm -rf $(BIN_PATH)
	rm -rf $(CACHE_PATH)
	rm -rf vendor
	rm -rf .env

composer-install: COMPOSER_COMMAND=install
composer-install: COMPOSER_OPTIONS=--ignore-platform-reqs
composer-install: composer

composer-dump-autoload: COMPOSER_COMMAND=dump-autoload
composer-dump-autoload: composer

composer: DOCKER_IMAGE=composer:latest
composer: $(BIN_PATH)/.docker composer.json
	$(DOCKER_COMMAND) php $(PHP_ARGS) /usr/bin/composer $(COMPOSER_COMMAND) $(COMPOSER_OPTIONS)

######################################################
#                     QA Commands                    #
######################################################

.PHONY: analyse cs-check cs-fix

# Run All Testing/QA
analyse: phpstan cs-check test

cs-check: DOCKER_IMAGE=twistersfury/php-cs-fixer
cs-check: DOCKER_OPTIONS=php-cs-fixer fix --verbose --dry-run --stop-on-violation --cache-file=$(CACHE_PATH)/.php_cs.cache
cs-check: $(CACHE_PATH) docker

cs-fix: DOCKER_IMAGE=twistersfury/php-cs-fixer
cs-fix: DOCKER_OPTIONS=php-cs-fixer fix --verbose --cache-file=$(CACHE_PATH)/.php_cs.cache
cs-fix: $(CACHE_PATH) docker

phpstan: DOCKER_IMAGE=twistersfury/phalcon:phpstan
phpstan: vendor docker

######################################################
#                  Testing Commands                  #
######################################################

.PHONY: analyse test test-unit test-acceptance codecept codecept-run codecept-dry-run codecept-build

# Run All Testing/QA
analyse: test


# Run All Testing
test: codecept-run

# Run Just The Acceptance Suite
test-acceptance: CODECEPT_SUB_COMMAND=acceptance
test-acceptance: codecept-run

# Run Just The Unit Suite
test-unit: CODECEPT_SUB_COMMAND=unit
test-unit: codecept-run

# Build Codecept
codecept-build: CODECEPT_COMMAND=build
codecept-build: codecept

codecept-run: CODECEPT_COMMAND=run
codecept-run: codecept

codecept-dry-run: CODECEPT_COMMAND=dry-run
codecept-dry-run: codecept

codecept: DOCKER_OPTIONS=php $(PHP_ARGS) $(PHP_EXTRA_ARGS) ./vendor/codeception/codeception/codecept $(CODECEPT_COMMAND) $(CODECEPT_SUB_COMMAND) $(CODECEPT_OPTIONS) $(TEST_COVERAGE)
codecept: vendor/codeception/codeception/codecept docker

######################################################
#                  Docker Commands                   #
######################################################

.PHONY: docker

docker:
	$(DOCKER_COMMAND) $(DOCKER_OPTIONS)